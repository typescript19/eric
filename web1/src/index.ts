import { default as express } from 'express';
import { default as morgan } from 'morgan';
import { default as cookieParser } from 'cookie-parser';
import * as auth from './auth';
import dotenv from 'dotenv';


const app = express();
dotenv.config();

// middleware
app.use(morgan('env'));
app.use(express.json());
app.use(cookieParser());

// routes
app.get('/', auth.index);
app.post('/login', auth.login);
app.get('/logout', auth.logout);

app.listen(process.env.PORT);