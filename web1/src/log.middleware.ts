import { Request, Response, NextFunction } from 'express';

export const logMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.log({
    header: req.headers,
    param: req.params,
    body: req.body,
    querystring: req.query,
  });
  next();
};